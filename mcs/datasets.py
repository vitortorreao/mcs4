#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Provides functions for reading the datasets. When executed as script, prints
useful information about each dataset in the resources folder."""

import csv
import math
import numpy as np


INVALID_ARGUMENT = """The argument {0} must be provided and it must be of type
 {1}."""

def parse_value(value):
    """Parses a value as float, integer or string.

    Parameters
    ----------
    value : str
        The value which should be parsed.

    Returns
    -------
    value : int, float or str
        The parsed value as an integer, float or string.
    """
    if not isinstance(value, str):
        raise ValueError("The provided value must be of type string.")
    value = value.strip()
    try:
        return int(value)
    except ValueError:
        pass
    try:
        return float(value)
    except ValueError:
        pass
    return value

def trigonometric_feature_expansion(objects):
    """Uses some math to expand the number of features.

    Parameters
    ----------
    objects : array-like, shape (n_samples, n_features)
        The original dataset.

    Returns
    -------
    new_objects : array-like, shape (n_samples, n_features * 7)
        The dataset expanded with six new features for each already existing
        feature.
    """
    new_objects = list()
    for i in range(objects.shape[0]):
        new_obj = list()
        for j in range(objects.shape[1]):
            new_obj.append(objects[i, j])
            new_obj.append(math.cos(math.pi * objects[i, j]))
            new_obj.append(math.sin(math.pi * objects[i, j]))
            new_obj.append(math.cos(2 * math.pi * objects[i, j]))
            new_obj.append(math.sin(2 * math.pi * objects[i, j]))
            new_obj.append(math.cos(3 * math.pi * objects[i, j]))
            new_obj.append(math.sin(3 * math.pi * objects[i, j]))
        new_objects.append(new_obj)
    return np.array(new_objects)

def convert_class_label(labels):
    """Converts the class labels to integers.

    Parameters
    ----------
    labels : list
        The list of labels (strings) to be converted to integers.

    Returns
    -------
    new_labels : list
        The list of integers which represent the labels.
    """
    if not isinstance(labels, list):
        raise ValueError("The list of labels must be a list.")
    if isinstance(labels[0], int):
        return labels
    class_map = dict()
    class_int = 0
    new_labels = list()
    for label in labels:
        if label not in class_map:
            class_map[label] = class_int
            class_int += 1
        new_labels.append(class_map[label])
    return new_labels

def read_dataset(filename, has_header=False, separator=",", class_column=-1,
                 has_missing=True):
    """Reads the dataset in the given filename.

    Parameters
    ----------
    filename : str
        A path to a file. This file is expected to contain a dataset with
        attributes separated by the given separator parameter.
    has_header : bool
        Whether or not the given dataset has a header line. If True, this
        function will ignore the first line of the file. Otherwise, will read
        every line in the file. False by default.
    separator : str
        This function expects the attributes in the file to be separated by
        this given string. Comma by default.
    class_column : int
        Which of the columns should be considered the label (or class) column.
        By default, the last column is considered as the class column. If None
        is provided, it means this dataset has no label column and is going to
        be used for unsupervised learning.
    has_missing : bool
        Whether or not the dataset has missing values. True by default.

    Returns
    -------
    X : np.ndarray
        A numpy matrix containing the each object in the rows and its features
        in the columns.
    y : np.ndarray
        A vector containing each of the object's expected class.
    """
    if not isinstance(filename, str):
        raise ValueError(INVALID_ARGUMENT.format("filename", "string"))
    if not isinstance(has_header, bool):
        raise ValueError(INVALID_ARGUMENT.format("has_header", "bool"))
    if not isinstance(separator, str):
        raise ValueError(INVALID_ARGUMENT.format("separator", "string"))
    if class_column != None and not isinstance(class_column, int):
        raise ValueError(INVALID_ARGUMENT.format("class_column", "integer"))
    if not isinstance(has_missing, bool):
        raise ValueError(INVALID_ARGUMENT.format("has_missing", "bool"))
    objects = list()
    labels = list()
    with open(filename, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=separator)
        if has_header:
            reader.next()
        size = -1
        for row in reader:
            if not has_missing:
                row = [obj for obj in row if obj]
            size = len(row)
            if class_column < 0:
                class_column = size + class_column
            obj = list()
            for i in range(size):
                if class_column == i:
                    labels.append(parse_value(row[i]))
                else:
                    obj.append(parse_value(row[i]))
            objects.append(obj)
    labels = convert_class_label(labels)
    return np.asarray(objects), np.asarray(labels)
