"""Implements algorithms for dynamic selection of classifiers and ensembles"""

import operator
import numpy as np
from sklearn.neighbors import NearestNeighbors
from .ensemble import BaggingBasic, FitMajorVotingEnsemble

class BaggingNeighborsSelectionBasic(BaggingBasic):
    """A class with a couple of methods common to all the k-Neighbors based
    dynamic selection algorithms"""

    def __init__(self, n_classifiers, k=5):
        super(BaggingNeighborsSelectionBasic, self).__init__(n_classifiers)
        self.k_neighbors = k

    def calculate_guesses(self, val_objects, val_labels):
        """Calculates the class given by each classifier on each validation
        example"""
        n_classifiers = len(self.classifiers)
        n_examples = val_objects.shape[0]
        self.classifiers_guess = np.zeros((n_examples, n_classifiers),
                                          dtype=np.bool)
        self.classifiers_classes = np.zeros((n_examples, n_classifiers),
                                            dtype=np.int)
        classifier_index = 0
        for classifier in self.classifiers:
            self.classifiers_classes[:, classifier_index] = \
                                                classifier.predict(val_objects)
            self.classifiers_guess[:, classifier_index] = \
                 self.classifiers_classes[:, classifier_index] == val_labels
            classifier_index += 1
        self.neighbors = NearestNeighbors(n_neighbors=self.k_neighbors)
        self.neighbors.fit(val_objects)

class BaggingLocalAccuracyBasic(BaggingNeighborsSelectionBasic):
    """Some basic methods for dynamic selection based on Local Accuracy"""

    def calculate_competences(self, object_, classifiers_answers):
        """Calculates the competence of each classifier for the given example"""
        # abstract method
        raise NotImplementedError()

    def predict(self, objects):
        labels = list()
        n_objects = objects.shape[0]
        n_classifiers = len(self.classifiers)
        classifiers_answers = np.zeros((n_objects, n_classifiers), dtype=np.int)
        for index in range(n_classifiers):
            classifiers_answers[:, index] = self.classifiers[index] \
                                                .predict(objects)
        for i in range(objects.shape[0]):
            object_ = objects[i, :].reshape(1, -1)
            if (classifiers_answers[i, :] == classifiers_answers[i, 0]).all():
                labels.append(classifiers_answers[i, 0])
                continue
            competences = self.calculate_competences(object_,
                                                     classifiers_answers[i, :])
            index, _ = max(enumerate(competences), key=operator.itemgetter(1))
            labels.append(classifiers_answers[i, index])
        return np.asarray(labels)

class BaggingOLA(BaggingLocalAccuracyBasic):
    """An ensemble with Overall Local Accuracy dynamic selection"""

    def calculate_competences(self, object_, classifiers_answers):
        """Calculates the competence of each classifier for the given example"""
        neighbors = self.neighbors.kneighbors(object_, return_distance=False)[0]
        competence = list()
        for classifier in range(self.classifiers_guess.shape[1]):
            guesses = self.classifiers_guess[neighbors, classifier]
            competence.append(np.sum(guesses))
        return competence

class BaggingLCA(BaggingLocalAccuracyBasic):
    """An Ensemble with Local Class Accuracy dynamic selection"""

    def calculate_competences(self, object_, classifiers_answers):
        """Calculates the competence of each classifier for the given example"""
        neighbors = self.neighbors.kneighbors(object_, return_distance=False)[0]
        competence = list()
        for index in range(len(self.classifiers)):
            total = 0.0
            correct = 0.0
            for neighbor in neighbors:
                if (self.classifiers_classes[neighbor, index] ==
                        classifiers_answers[index]):
                    total += 1.0
                    if self.classifiers_guess[neighbor, index]:
                        correct += 1.0
            competence.append(correct/total if total > 0.0 else 0.0)
        return competence

class BaggingKNORAE(BaggingNeighborsSelectionBasic):
    """An Ensemble with KNORA-E dynamic selection"""

    def predict(self, objects):
        labels = list()
        for i in range(objects.shape[0]):
            object_ = objects[i, :].reshape(1, -1)
            k_neighbors = self.k_neighbors + 1
            all_correct_classifiers = list()
            while len(all_correct_classifiers) < 1 and k_neighbors > 1:
                k_neighbors -= 1
                neighbors = self.neighbors.kneighbors(object_,
                                                      n_neighbors=k_neighbors,
                                                      return_distance=False)[0]
                for index in range(len(self.classifiers)):
                    if self.classifiers_guess[neighbors, index].all():
                        all_correct_classifiers.append(self.classifiers[index])
            if len(all_correct_classifiers) < 1:
                all_correct_classifiers = self.classifiers
            ensemble = FitMajorVotingEnsemble(all_correct_classifiers)
            labels.append(ensemble.predict(object_)[0])
        return np.asarray(labels)
