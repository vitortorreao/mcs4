#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Provides a simple implementations for ensembling classifiers."""

import math
import random
import numpy as np

from .diversity import DIVERSITY_METRIC_MAP, MIN_DIVERSITY_METRIC_MAP

def bootstrap_sample(objects, labels, new_size=None):
    """Creates a Bootstrap sample from the given training set.

    Parameters
    ----------
    objects : array-like, shape = (n_samples, n_features)
        The objects in the training set (without the class labels).
    labels : array-like, shape = (n_samples,)
        The labels for the objects in the training set.
    new_size : int
        The number of objects in the output sample. Defaults to same as input.

    Returns
    -------
    objects : array-like, shape = (new_size, n_features)
        The bootstrapped sample.
    labels : array-like, shape = (new_size,)
        The labels for the objects in the bootstrapped sample.
    """
    if new_size != None and not isinstance(new_size, int):
        raise ValueError("The argument 'new_size' should be an integer.")
    if not new_size:
        new_size = objects.shape[0]
    inds = list(range(objects.shape[0]))
    sample_inds = np.array([random.choice(inds) for _ in range(new_size)])
    return objects[sample_inds], labels[sample_inds]

def select_random_subspace(features_indexes, percentage=0.5):
    """Selects a subset of the features (with replacement).

    Parameters
    ----------
    features_indexes : list
        A list of integers (indexes) containing all the indexes to the features.

    Returns
    -------
    new_features_indexes : list
        A list of integers (indexes) of the selected features. Note that
        len(new_features_indexes) < len(features_indexes).
    """
    random.shuffle(features_indexes) # shuffle happens in-place.
    upper = int(math.floor(len(features_indexes) * percentage))
    return features_indexes[:upper]

class EnsembleBasic(object):
    """Represents a basic ensemble skeleton."""

    def __init__(self, n_classifiers=1, classifier=None):
        """Creates n_classifiers instances.

        Parameters
        ----------
        n_classifiers : int
            The number of classifiers in the pool.
        classifier : object
            The classifier class. Will generate n_classifiers copies of this for
            the pool. Defaults to None, so watch out.
        """
        self.classifiers_features = [None] * n_classifiers
        if classifier is None:
            self.classifiers = [None] * n_classifiers
            return
        self.classifiers = list()
        for _ in range(n_classifiers):
            self.classifiers.append(classifier())

    def diversity_score(self, examples, labels, metric="yule"):
        """Calculates a diversity score for the ensemble.

        Parameters
        ----------
        examples : array-like, shape = (n_samples, n_features)
            The examples in the test or validation sets.
        labels : array-like, shape = (n_samples,)
            The correct labels for the given examples.
        metric : str
            The name of the diversity metric that should be calculated. Consult
            the diversity metric map to find available metrics. Defaults to
            Yule's Q statistic.

        Returns
        -------
        score : float
            The diversity score calculated using the diversity metric.
        """
        if not isinstance(metric, str) or metric not in DIVERSITY_METRIC_MAP:
            raise ValueError(("The 'metric' parameter must be a string " +
                              "corresponding to one of the diversity metrics." +
                              " {0} is not an available metric.")
                             .format(str(metric)))
        if len(self.classifiers) < 2: # if there is only one classifier,
            # return worst diversity score possible
            return MIN_DIVERSITY_METRIC_MAP[metric]
        class_Ns = list()
        for classifier in self.classifiers:
            y_pred = classifier.predict(examples)
            class_N = list()
            for i in range(labels.shape[0]):
                if y_pred[i] == labels[i]:
                    class_N.append(1)
                else:
                    class_N.append(0)
            class_Ns.append(class_N)
        class_Ns = np.asarray(class_Ns)
        return DIVERSITY_METRIC_MAP[metric](class_Ns)

class MajorVotingEnsemble(EnsembleBasic):
    """Provides a basic implementation of major voting integration for MCSs."""

    def predict(self, objects):
        """Predicts the classes for all the objects in X using the class with
        the highest number of votes.

        Parameters
        ----------
        objects : array-like, shape = (n_samples, n_features)
            The objects which must be classified.

        Returns
        -------
        labels : array-like, shape = (n_samples,)
            The labels for each of the given objects.
        """
        instances = [dict() for _ in range(objects.shape[0])]
        classifier_index = 0
        for classifier in self.classifiers:
            features = []
            if not self.classifiers_features[classifier_index]:
                features = range(objects.shape[1])
            else:
                features = self.classifiers_features[classifier_index]
            labels = classifier.predict(objects[:, features])
            for i in range(objects.shape[0]):
                if labels[i] not in instances[i]:
                    instances[i][labels[i]] = 0
                instances[i][labels[i]] += 1
            classifier_index += 1
        classes = list()
        for instance_classes in instances:
            output_class = max(instance_classes,
                               key=lambda key: instance_classes[key])
            classes.append(output_class)
        return np.array(classes)

class BaggingBasic(MajorVotingEnsemble):
    """Provides the basic methods to implement a Bagging generated MCS."""

    def fit(self, objects, labels):
        """Fits the classifiers to the given training data.

        Parameters
        ----------
        objects : array-like, shape = (n_samples, n_features)
            The objects in the training set, which will be used to fit the
            classifier.
        labels : array-like, shape = (n_samples,)
            The expected labels for the objects in the training set.
        """
        for classifier in self.classifiers:
            objects2, labels2 = bootstrap_sample(objects, labels)
            classifier.fit(objects2, labels2)

class RandomSubspaceBasic(MajorVotingEnsemble):
    """Provides the basic methods to implement a Random Subspace generated
       MCS."""

    def __init__(self, n_classifiers=1, classifier=None,
                 features_percentage=0.5):
        """Prepares for the generation of an ensemble of classifiers using the
        random subspace algorithm, with n_classifiers classifiers in the pool
        and features_percentage % of the features for each.

        Parameters
        ----------
        n_classifiers : int
            The number of classifiers in the pool.
        classifier : object
            The classifier class. Will generate n_classifiers copies of this for
            the pool. Defaults to None, so watch out.
        features_percentage : float
            The percentage of the features that will be used by each classifier.
        """
        super(RandomSubspaceBasic, self).__init__(n_classifiers, classifier)
        self.features_percentage = features_percentage

    def fit(self, objects, labels):
        """Fits the classifiers in the pool to the given training data.

        Parameters
        ----------
        objects : array-like, shape = (n_samples, n_features)
            The objects in the training set, which will be used to fit the
            classifier.
        labels : array-like, shape = (n_samples,)
            The expected labels for the objects in the training set.
        """
        classifier_index = 0
        for classifier in self.classifiers:
            features = select_random_subspace(range(objects.shape[1]),
                                              self.features_percentage)
            self.classifiers_features[classifier_index] = features
            classifier.fit(objects[:, features], labels)
            classifier_index += 1

class FitMajorVotingEnsemble(MajorVotingEnsemble):
    """An ensemble of already fit classifiers combined using major voting"""

    def __init__(self, classifiers, classifiers_features=None):
        super(FitMajorVotingEnsemble, self).__init__(len(classifiers))
        self.classifiers = classifiers
        if classifiers_features:
            self.classifiers_features = classifiers_features
