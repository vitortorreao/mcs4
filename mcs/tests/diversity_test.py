#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests the diversity metrics implementations."""

import unittest
import numpy as np

from ..diversity import relationship_table, yule_diversity_pair_wise, \
                        q_average, double_fault_pair_wise, entropy_measure, \
                        kohavi_wolpert, disagreement_measure_pair_wise

class TestYuleDiversityPairWise(unittest.TestCase):
    """Tests the pair-wise implementation of Yule's diversity"""

    def test_yule_diversity_pair_wise(self):
        """Tests the pair-wise version of the Yule diversity metric"""
        diverse = np.array([
            [1, 1, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 1]
        ])
        score = yule_diversity_pair_wise(diverse, 0, 1)
        self.assertEqual(-1.0, score)
        score2 = yule_diversity_pair_wise(diverse, 1, 0)
        self.assertEqual(score, score2)
        not_diverse = np.array([
            [1, 0, 1, 1, 1, 1],
            [1, 0, 1, 1, 1, 1]
        ])
        score = yule_diversity_pair_wise(not_diverse, 0, 1)
        self.assertEqual(1.0, score)
        score2 = yule_diversity_pair_wise(not_diverse, 1, 0)
        self.assertEqual(score, score2)
        outputs = np.array([
            [0, 1, 0, 1, 0, 0],
            [0, 1, 0, 0, 1, 1]
        ])
        score = yule_diversity_pair_wise(outputs, 0, 1)
        self.assertEqual(0.0, score)
        score2 = yule_diversity_pair_wise(outputs, 1, 0)
        self.assertEqual(score, score2)
        outputs = np.array([
            [0, 1, 0, 1, 0, 0, 0],
            [0, 1, 0, 0, 1, 1, 0]
        ])
        score = yule_diversity_pair_wise(outputs, 0, 1)
        self.assertEqual(0.2, score)
        score2 = yule_diversity_pair_wise(outputs, 1, 0)
        self.assertEqual(score, score2)

class TestDoubleFaultDiversityPairWise(unittest.TestCase):
    """Tests the pair-wise implementation of double fault diversity"""

    def test_double_fault_pair_wise(self):
        """Tests the pair-wise version of the double fault diversity"""
        diverse = np.array([
            [1, 1, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 1]
        ])
        score = double_fault_pair_wise(diverse, 0, 1)
        self.assertEqual(0.0, score)
        score2 = double_fault_pair_wise(diverse, 1, 0)
        self.assertEqual(score, score2)
        not_diverse = np.array([
            [1, 0, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1]
        ])
        score = double_fault_pair_wise(not_diverse, 0, 1)
        self.assertEqual(0.0, score)
        score2 = double_fault_pair_wise(not_diverse, 1, 0)
        self.assertEqual(score, score2)
        outputs = np.array([
            [0, 1, 0, 1, 0, 0],
            [0, 1, 0, 0, 1, 0]
        ])
        score = double_fault_pair_wise(outputs, 0, 1)
        self.assertEqual(0.5, score)
        score2 = double_fault_pair_wise(outputs, 1, 0)
        self.assertEqual(score, score2)
        outputs = np.array([
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0]
        ])
        score = double_fault_pair_wise(outputs, 0, 1)
        self.assertEqual(1.0, score)
        score2 = double_fault_pair_wise(outputs, 1, 0)
        self.assertEqual(score, score2)

class TestDisagreementMeasurePairWise(unittest.TestCase):
    """Tests the pair-wise implementation of the disagreement measure"""

    def test_disagreement_measure_pair_wise(self):
        """Tests the pair-wise implementation of the disagreement measure"""
        diverse = np.array([
            [1, 1, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 1]
        ])
        score = disagreement_measure_pair_wise(diverse, 0, 1)
        self.assertEqual(1.0, score)
        score2 = disagreement_measure_pair_wise(diverse, 1, 0)
        self.assertEqual(score, score2)
        not_diverse = np.array([
            [1, 0, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1]
        ])
        score = disagreement_measure_pair_wise(not_diverse, 0, 1)
        self.assertEqual(1.0/6.0, score)
        score2 = disagreement_measure_pair_wise(not_diverse, 1, 0)
        self.assertEqual(score, score2)
        outputs = np.array([
            [0, 1, 0, 1, 0, 0],
            [0, 1, 0, 0, 1, 0]
        ])
        score = disagreement_measure_pair_wise(outputs, 0, 1)
        self.assertEqual(2.0/6.0, score)
        score2 = disagreement_measure_pair_wise(outputs, 1, 0)
        self.assertEqual(score, score2)
        outputs = np.array([
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0]
        ])
        score = disagreement_measure_pair_wise(outputs, 0, 1)
        self.assertEqual(0.0, score)
        score2 = disagreement_measure_pair_wise(outputs, 1, 0)
        self.assertEqual(score, score2)

class TestQAverage(unittest.TestCase):
    """Tests the Q average function"""

    def test_q_average(self):
        """Tests the Q average implementation"""
        outputs = np.array([
            [1, 1, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 1]
        ])
        score = q_average(outputs, yule_diversity_pair_wise)
        self.assertEqual(-1.0, score)
        outputs = np.array([
            [1, 1, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 1],
            [1, 1, 1, 1, 1, 0]
        ])
        score = q_average(outputs, yule_diversity_pair_wise)
        # score should be -0.33, but we must account for float-point inacurracy
        self.assertTrue(score < -0.3333)
        self.assertTrue(score > -0.3334)

class TestEntropyDiversity(unittest.TestCase):
    """Tests the Entropy ensemble diversity measure"""

    def test_entropy_measure(self):
        """Tests the entropy diversity measure for the whole ensemble"""
        diverse = np.array([
            [1, 1, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 1]
        ])
        score = entropy_measure(diverse)
        self.assertEqual(1.0, score)
        not_diverse = np.array([
            [1, 0, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1]
        ])
        score = entropy_measure(not_diverse)
        self.assertEqual(1.0/6.0, score)
        outputs = np.array([
            [0, 1, 0, 1, 0, 0],
            [0, 1, 0, 0, 1, 0],
            [1, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0]
        ])
        score = entropy_measure(outputs)
        self.assertEqual(4.0/6.0, score)

class TestKohaviWolpertDiversity(unittest.TestCase):
    """Tests the Kohavi Wolpert diversity measure"""

    def test_kohavi_wolpert(self):
        """Tests the kohavi wolpert diversity measure for the whole ensemble"""
        diverse = np.array([
            [1, 1, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 1]
        ])
        score = kohavi_wolpert(diverse)
        self.assertEqual(1.0/4.0, score)
        not_diverse = np.array([
            [1, 0, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1]
        ])
        score = kohavi_wolpert(not_diverse)
        self.assertEqual(1.0/24.0, score)
        outputs = np.array([
            [0, 1, 0, 1, 0, 0],
            [0, 1, 0, 0, 1, 0],
            [1, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0]
        ])
        score = kohavi_wolpert(outputs)
        self.assertEqual(20.0/96.0, score)

class TestRelationshipTable(unittest.TestCase):
    """Tests the relationship_table function"""

    def test_relationship_table(self):
        """tests the relationship_table"""
        n_tab = relationship_table(np.array([
            [1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1]
        ]), 0, 1)
        self.assertEqual(0, n_tab[0, 0])
        self.assertEqual(0, n_tab[0, 1])
        self.assertEqual(0, n_tab[1, 0])
        self.assertEqual(6, n_tab[1, 1])
        n_tab = relationship_table(np.array([
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0]
        ]), 0, 1)
        self.assertEqual(6, n_tab[0, 0])
        self.assertEqual(0, n_tab[0, 1])
        self.assertEqual(0, n_tab[1, 0])
        self.assertEqual(0, n_tab[1, 1])
        n_tab = relationship_table(np.array([
            [1, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0]
        ]), 0, 1)
        self.assertEqual(0, n_tab[0, 0])
        self.assertEqual(0, n_tab[0, 1])
        self.assertEqual(6, n_tab[1, 0])
        self.assertEqual(0, n_tab[1, 1])
        n_tab = relationship_table(np.array([
            [0, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1]
        ]), 0, 1)
        self.assertEqual(0, n_tab[0, 0])
        self.assertEqual(6, n_tab[0, 1])
        self.assertEqual(0, n_tab[1, 0])
        self.assertEqual(0, n_tab[1, 1])
        n_tab = relationship_table(np.array([
            [1, 1, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 1]
        ]), 0, 1)
        self.assertEqual(0, n_tab[0, 0])
        self.assertEqual(3, n_tab[0, 1])
        self.assertEqual(3, n_tab[1, 0])
        self.assertEqual(0, n_tab[1, 1])
        n_tab = relationship_table(np.array([
            [0, 0, 0, 1, 1, 1],
            [1, 1, 1, 0, 0, 0]
        ]), 0, 1)
        self.assertEqual(0, n_tab[0, 0])
        self.assertEqual(3, n_tab[0, 1])
        self.assertEqual(3, n_tab[1, 0])
        self.assertEqual(0, n_tab[1, 1])
        n_tab = relationship_table(np.array([
            [1, 1, 1, 1, 0, 0],
            [1, 0, 0, 1, 1, 0]
        ]), 0, 1)
        self.assertEqual(1, n_tab[0, 0])
        self.assertEqual(1, n_tab[0, 1])
        self.assertEqual(2, n_tab[1, 0])
        self.assertEqual(2, n_tab[1, 1])

    def test_relationship_indexing(self):
        """Tests if the relationship_table is correctly taking the index into
        consideration"""
        n_tab = relationship_table(np.array([
            [0, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1]
        ]), 1, 2)
        self.assertEqual(0, n_tab[0, 0])
        self.assertEqual(0, n_tab[0, 1])
        self.assertEqual(0, n_tab[1, 0])
        self.assertEqual(6, n_tab[1, 1])
        n_tab = relationship_table(np.array([
            [0, 0, 0, 0, 0, 0],
            [1, 1, 0, 1, 0, 1],
            [0, 0, 0, 0, 0, 0]
        ]), 0, 2)
        self.assertEqual(6, n_tab[0, 0])
        self.assertEqual(0, n_tab[0, 1])
        self.assertEqual(0, n_tab[1, 0])
        self.assertEqual(0, n_tab[1, 1])
        n_tab = relationship_table(np.array([
            [1, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0],
            [1, 0, 1, 0, 1, 0]
        ]), 0, 1)
        self.assertEqual(0, n_tab[0, 0])
        self.assertEqual(0, n_tab[0, 1])
        self.assertEqual(6, n_tab[1, 0])
        self.assertEqual(0, n_tab[1, 1])
        n_tab = relationship_table(np.array([
            [0, 1, 0, 1, 1, 0],
            [0, 0, 0, 0, 0, 0],
            [1, 0, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1]
        ]), 1, 3)
        self.assertEqual(0, n_tab[0, 0])
        self.assertEqual(6, n_tab[0, 1])
        self.assertEqual(0, n_tab[1, 0])
        self.assertEqual(0, n_tab[1, 1])
        n_tab = relationship_table(np.array([
            [0, 0, 0, 0, 0, 0],
            [1, 1, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 1],
            [1, 0, 0, 1, 1, 0]
        ]), 1, 2)
        self.assertEqual(0, n_tab[0, 0])
        self.assertEqual(3, n_tab[0, 1])
        self.assertEqual(3, n_tab[1, 0])
        self.assertEqual(0, n_tab[1, 1])
        n_tab = relationship_table(np.array([
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 1, 1],
            [1, 1, 1, 1, 1, 1],
            [1, 1, 1, 0, 0, 0],
            [1, 0, 1, 1, 1, 0]
        ]), 1, 3)
        self.assertEqual(0, n_tab[0, 0])
        self.assertEqual(3, n_tab[0, 1])
        self.assertEqual(3, n_tab[1, 0])
        self.assertEqual(0, n_tab[1, 1])

if __name__ == "__main__":
    unittest.main()
