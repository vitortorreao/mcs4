#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests the dataset functions."""

import unittest
import numpy as np

from ..datasets import trigonometric_feature_expansion

class TestTrigonometricExpansion(unittest.TestCase):
    """Test cases for trigonometric expansion."""

    def test_trig_feature_expansion(self):
        """Tests the trigonometric expansion function."""
        new_objects = trigonometric_feature_expansion(np.array([
            [0, 1, 2, 3, 4],
            [5, 6, 7, 8, 9]
        ]))
        self.assertEqual(2, new_objects.shape[0])
        self.assertEqual(35, new_objects.shape[1])
        self.assertTrue(0 in new_objects[0, :])
        self.assertTrue(1 in new_objects[0, :])
        self.assertTrue(2 in new_objects[0, :])
        self.assertTrue(3 in new_objects[0, :])
        self.assertTrue(4 in new_objects[0, :])
        self.assertTrue(5 in new_objects[1, :])
        self.assertTrue(6 in new_objects[1, :])
        self.assertTrue(7 in new_objects[1, :])
        self.assertTrue(8 in new_objects[1, :])
        self.assertTrue(9 in new_objects[1, :])

if __name__ == "__main__":
    unittest.main()
