#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests the ensemble implementations."""

import unittest
import numpy as np

from mock import Mock

from ..ensemble import EnsembleBasic
from ..pruning import count_votes, get_predictions, \
                      get_individual_contribution, epic_pruning

class EPICPruningTest(unittest.TestCase):
    """Tests for the epic pruning function and its helpers"""

    def setUp(self):
        self.classifier1 = Mock()
        self.classifier1.predict.return_value = np.asarray([1, 1, 2, 3, 2])
        self.classifier2 = Mock()
        self.classifier2.predict.return_value = np.asarray([1, 2, 2, 1, 3])
        self.classifier3 = Mock()
        self.classifier3.predict.return_value = np.asarray([1, 2, 3, 3, 1])
        self.classifier4 = Mock()
        self.classifier4.predict.return_value = np.asarray([1, 3, 2, 3, 2])
        self.classifier5 = Mock()
        self.classifier5.predict.return_value = np.asarray([1, 2, 3, 2, 3])
        self.ensemble = EnsembleBasic(n_classifiers=5)
        self.ensemble.classifiers = [self.classifier1, self.classifier2,
                                     self.classifier3, self.classifier4,
                                     self.classifier5]
        self.validation_examples = np.asarray([[0.5, 0.2, 0.1],
                                               [0.2, 0.7, 0.3],
                                               [0.7, 0.8, 0.9],
                                               [0.1, 0.05, 0.2],
                                               [1.2, 1.3, 1.1]])
        self.validation_labels = np.asarray([1, 1, 3, 3, 2])
        self.classes = np.asarray([1, 2, 3])
        self.class_index_map = {1: 0, 2: 1, 3: 2}

    def test_get_indiv_contribution(self):
        """tests the calculation of the individual contributions of each
        classifier in each data sample"""
        predictions = get_predictions(self.ensemble, self.validation_examples)
        votes = count_votes(predictions, self.classes, self.class_index_map)
        n_samples = predictions.shape[0]
        n_classifiers = predictions.shape[1]
        indivc = np.zeros((n_samples, n_classifiers), dtype=np.float)
        for i in range(n_samples):
            for j in range(n_classifiers):
                indivc[i, j] = \
                    get_individual_contribution(i, j, votes, predictions,
                                                self.classes,
                                                self.validation_labels,
                                                self.class_index_map)
        self.assertEqual(0, indivc[0, 0])
        self.assertEqual(0, indivc[0, 1])
        self.assertEqual(0, indivc[0, 2])
        self.assertEqual(0, indivc[0, 3])
        self.assertEqual(0, indivc[0, 4])

        self.assertEqual(5, indivc[1, 0])
        self.assertEqual(-5, indivc[1, 1])
        self.assertEqual(-5, indivc[1, 2])
        self.assertEqual(-3, indivc[1, 3])
        self.assertEqual(-5, indivc[1, 4])

        self.assertEqual(-4, indivc[2, 0])
        self.assertEqual(-4, indivc[2, 1])
        self.assertEqual(4, indivc[2, 2])  # 0
        self.assertEqual(-4, indivc[2, 3])
        self.assertEqual(4, indivc[2, 4]) # 0

        self.assertEqual(1, indivc[3, 0])
        self.assertEqual(-1, indivc[3, 1])
        self.assertEqual(1, indivc[3, 2])
        self.assertEqual(1, indivc[3, 3])
        self.assertEqual(-1, indivc[3, 4])

        self.assertEqual(2, indivc[4, 0])
        self.assertEqual(-2, indivc[4, 1])
        self.assertEqual(-1, indivc[4, 2])
        self.assertEqual(2, indivc[4, 3])
        self.assertEqual(-2, indivc[4, 4])

    def test_get_predictions(self):
        """tests the calculation of classifiers predictions per sample"""
        predictions = get_predictions(self.ensemble, self.validation_examples)
        self.assertEqual(1, predictions[0, 0])
        self.assertEqual(1, predictions[1, 0])
        self.assertEqual(2, predictions[2, 0])
        self.assertEqual(3, predictions[3, 0])
        self.assertEqual(2, predictions[4, 0])

        self.assertEqual(1, predictions[0, 1])
        self.assertEqual(2, predictions[1, 1])
        self.assertEqual(2, predictions[2, 1])
        self.assertEqual(1, predictions[3, 1])
        self.assertEqual(3, predictions[4, 1])

        self.assertEqual(1, predictions[0, 2])
        self.assertEqual(2, predictions[1, 2])
        self.assertEqual(3, predictions[2, 2])
        self.assertEqual(3, predictions[3, 2])
        self.assertEqual(1, predictions[4, 2])

        self.assertEqual(1, predictions[0, 3])
        self.assertEqual(3, predictions[1, 3])
        self.assertEqual(2, predictions[2, 3])
        self.assertEqual(3, predictions[3, 3])
        self.assertEqual(2, predictions[4, 3])

        self.assertEqual(1, predictions[0, 4])
        self.assertEqual(2, predictions[1, 4])
        self.assertEqual(3, predictions[2, 4])
        self.assertEqual(2, predictions[3, 4])
        self.assertEqual(3, predictions[4, 4])

    def test_count_votes(self):
        """Tests the vote count helper function"""
        predictions = get_predictions(self.ensemble, self.validation_examples)
        votes = count_votes(predictions, self.classes, self.class_index_map)
        self.assertEqual(5, votes.shape[0])
        self.assertEqual(3, votes.shape[1])
        self.assertEqual(5, votes[0, 0])
        self.assertEqual(0, votes[0, 1])
        self.assertEqual(0, votes[0, 2])
        self.assertEqual(1, votes[1, 0])
        self.assertEqual(3, votes[1, 1])
        self.assertEqual(1, votes[1, 2])
        self.assertEqual(0, votes[2, 0])
        self.assertEqual(3, votes[2, 1])
        self.assertEqual(2, votes[2, 2])
        self.assertEqual(1, votes[3, 0])
        self.assertEqual(1, votes[3, 1])
        self.assertEqual(3, votes[3, 2])
        self.assertEqual(1, votes[4, 0])
        self.assertEqual(2, votes[4, 1])
        self.assertEqual(2, votes[4, 2])

    def test_epic_prunning(self):
        """tests the EPIC prunning"""
        epic_pruning(self.ensemble, 0.2, self.validation_examples,
                     self.validation_labels)
        self.assertEqual(1, len(self.ensemble.classifiers))
        self.assertEqual(self.classifier1, self.ensemble.classifiers[0])
        self.ensemble.classifiers = [self.classifier1, self.classifier2,
                                     self.classifier3, self.classifier4,
                                     self.classifier5]
        epic_pruning(self.ensemble, 0.4, self.validation_examples,
                     self.validation_labels)
        self.assertEqual(2, len(self.ensemble.classifiers))
        self.assertEqual(self.classifier1, self.ensemble.classifiers[0])
        self.assertEqual(self.classifier3, self.ensemble.classifiers[1])
