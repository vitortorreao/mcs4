"""Tests the selection module"""

import unittest
import numpy as np

from mock import Mock, MagicMock

from ..selection import BaggingOLA, BaggingLCA, BaggingKNORAE

class TestBaggingOLA(unittest.TestCase):
    """Tests the BaggingOLA class"""

    def setUp(self):
        """Sets up the test class"""
        self.val_objects = np.asarray([
            [1, 0, 1],
            [0, 1, 0],
            [0, 1, 1],
            [0, 0, 1],
            [1, 1, 0]
        ])
        self.val_labels = np.asarray([1, 2, 2, 1, 2])
        self.classifier1 = Mock()
        self.classifier1.predict.return_value = np.asarray([2, 2, 1, 2, 2])
        self.classifier2 = Mock()
        self.classifier2.predict.return_value = np.asarray([1, 2, 1, 1, 2])
        self.classifier3 = Mock()
        self.classifier3.predict.return_value = np.asarray([2, 2, 2, 1, 1])
        self.ola = BaggingOLA(3, 5)
        self.ola.classifiers = [self.classifier1, self.classifier2,
                                self.classifier3]
        self.ola.calculate_guesses(self.val_objects, self.val_labels)

    def test_calculate_guesses(self):
        """Tests the calculate_guesses method"""
        classifier1_expected = np.asarray([False, True, False, False, True],
                                          dtype=np.bool)
        classifier2_expected = np.asarray([True, True, False, True, True],
                                          dtype=np.bool)
        classifier3_expected = np.asarray([False, True, True, True, False],
                                          dtype=np.bool)
        self.assertTrue(
            (classifier1_expected == self.ola.classifiers_guess[:, 0]).all()
        )
        self.assertTrue(
            (classifier2_expected == self.ola.classifiers_guess[:, 1]).all()
        )
        self.assertTrue(
            (classifier3_expected == self.ola.classifiers_guess[:, 2]).all()
        )
        self.assertTrue(self.ola.neighbors)
        self.assertTrue(
            (self.classifier1.predict.return_value ==
             self.ola.classifiers_classes[:, 0]).all()
        )
        self.assertTrue(
            (self.classifier2.predict.return_value ==
             self.ola.classifiers_classes[:, 1]).all()
        )
        self.assertTrue(
            (self.classifier3.predict.return_value ==
             self.ola.classifiers_classes[:, 2]).all()
        )

    def test_calculate_competence(self):
        """Tests the calculate_competence method"""
        competences = self.ola.calculate_competences(
            np.asarray([1, 0, 0]).reshape(1, -1),
            np.asarray([1, 2, 1])
        )
        self.assertEqual(3, len(competences))
        self.assertEqual(2, competences[0])
        self.assertEqual(4, competences[1])
        self.assertEqual(3, competences[2])

    def test_predict(self):
        """Tests the predict method"""
        classifier1 = Mock()
        classifier1.predict.return_value = np.asarray([4])
        classifier2 = Mock()
        classifier2.predict.return_value = np.asarray([5])
        classifier3 = Mock()
        classifier3.predict.return_value = np.asarray([6])
        self.ola.classifiers = [classifier1, classifier2, classifier3]
        labels = self.ola.predict(np.asarray([
            [1, 0, 1]
        ]))
        self.assertEqual(5, labels[0])

        # test the concesus
        classifier1.predict.return_value = np.asarray([4])
        classifier2.predict.return_value = np.asarray([4])
        classifier3.predict.return_value = np.asarray([4])
        self.ola.classifiers = [classifier1, classifier2, classifier3]
        labels = self.ola.predict(np.asarray([[1, 0, 1]]))
        self.assertEqual(4, labels[0])

class TestBaggingLCA(unittest.TestCase):
    """Tests the BaggingLCA class"""

    def setUp(self):
        self.val_objects = np.asarray([
            [1, 0, 1],
            [0, 1, 0],
            [0, 1, 1],
            [0, 0, 1],
            [1, 1, 0]
        ])
        self.val_labels = np.asarray([1, 2, 2, 1, 2])
        self.classifier1 = Mock()
        self.classifier1.predict.return_value = np.asarray([2, 2, 1, 2, 2])
        self.classifier2 = Mock()
        self.classifier2.predict.return_value = np.asarray([1, 2, 1, 1, 2])
        self.classifier3 = Mock()
        self.classifier3.predict.return_value = np.asarray([2, 2, 2, 1, 1])
        self.lca = BaggingLCA(3, 5)
        self.lca.classifiers = [self.classifier1, self.classifier2,
                                self.classifier3]
        self.lca.calculate_guesses(self.val_objects, self.val_labels)

    def test_calculate_competences(self):
        """Tests the calculate_competences method"""
        competences = self.lca.calculate_competences(
            np.asarray([1, 0, 0]).reshape(1, -1),
            np.asarray([1, 2, 1])
        )
        self.assertEqual(3, len(competences))
        self.assertEqual(0.0, competences[0])
        self.assertEqual(1.0, competences[1])
        self.assertEqual(0.5, competences[2])

    def test_predict(self):
        """Tests the predict method"""
        classifier1 = Mock()
        classifier1.predict.return_value = np.asarray([1])
        classifier2 = Mock()
        classifier2.predict.return_value = np.asarray([2])
        classifier3 = Mock()
        classifier3.predict.return_value = np.asarray([1])
        self.lca.classifiers = [classifier1, classifier2, classifier3]
        labels = self.lca.predict(np.asarray([
            [1, 0, 1]
        ]))
        self.assertEqual(2, labels[0])

        # test the concesus
        classifier1.predict.return_value = np.asarray([3])
        classifier2.predict.return_value = np.asarray([3])
        classifier3.predict.return_value = np.asarray([3])
        self.lca.classifiers = [classifier1, classifier2, classifier3]
        labels = self.lca.predict(np.asarray([[1, 0, 1]]))
        self.assertEqual(3, labels[0])

class TestbaggingKNORAE(unittest.TestCase):
    """Tests the BaggingKNORAE class"""

    def test_predict(self):
        """Tests the predict method"""
        val_objects = np.asarray([
            [1, 0, 1],
            [0, 1, 0],
            [0, 1, 1],
            [0, 0, 1],
            [1, 1, 0]
        ])
        val_labels = np.asarray([1, 2, 2, 1, 2])
        classifier1 = Mock()
        classifier1.predict.return_value = np.asarray([2, 2, 1, 2, 2])
        classifier2 = Mock()
        classifier2.predict.return_value = np.asarray([1, 2, 1, 1, 2])
        classifier3 = Mock()
        classifier3.predict.return_value = np.asarray([2, 2, 2, 1, 1])
        knorae = BaggingKNORAE(3, 5)
        knorae.classifiers = [classifier1, classifier2,
                           classifier3]
        knorae.calculate_guesses(val_objects, val_labels)
        knorae.neighbors.kneighbors = MagicMock(
            side_effect=lambda x, n_neighbors, return_distance: \
                                                    val_objects[:n_neighbors, :]
        )
        classifier2.predict.return_value = np.asarray([3])
        knorae.classifiers = [classifier1, classifier2, classifier3]
        labels = knorae.predict(np.asarray([[1, 0, 1], [1, 1, 1]]))
        self.assertEqual(2, len(labels))
        self.assertEqual(3, labels[0])
        self.assertEqual(3, labels[1])
