#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Provides a simple implementations for calculating ensemble diversity."""

import math
import numpy as np

# Helpers

def relationship_table(classifiers_outputs, ind_i, ind_k):
    """Calculates the 2x2 relationship table N. Where N[0,0] is the number of
    examples where both classifier i and k are wrong; N[0,1] is the number of
    examples where classifier i is wrong, but classifier k is right; N[1,0] is
    the number of examples where classifier k is wrong, but classifier i is
    right; N[1,1] is the number of examples where both classifier i and k are
    right.

    Parameters
    ----------
    classifiers_outputs : array-like, shape = (n_classifier, n_samples)
        The N arrays for each classifier as defined in
        (Kuncheva & Whitaker, 2003), https://doi.org/10.1023/A:102285900
    ind_i : int
        The index of the first classifier in the classifiers_outputs array.
    ind_k : int
        The index of the second classifier in the classifiers_outputs array.

    Returns
    -------
    n_table : array-like, shape = (2, 2)
        A 2x2 matrix containing the relationship between classifiers i and k.
        n_table[0, 0] is the number of examples where both are wrong,
        n_table[0, 1] is the number of examples where i is wrong, but k is
        right, n_table[1, 0] is the number of examples where i is right, but k
        is wrong, and n_table[1, 1] is the number of examples where both are
        right.
    """
    n_table = np.zeros((2, 2), dtype=np.int64)
    for j in range(classifiers_outputs.shape[1]):
        if (classifiers_outputs[ind_i, j] == 1 and
                classifiers_outputs[ind_k, j] == 1):
            n_table[1, 1] += 1
        elif (classifiers_outputs[ind_i, j] == 1 and
              classifiers_outputs[ind_k, j] == 0):
            n_table[1, 0] += 1
        elif (classifiers_outputs[ind_i, j] == 0 and
              classifiers_outputs[ind_k, j] == 1):
            n_table[0, 1] += 1
        else:
            n_table[0, 0] += 1
    return n_table

def q_average(classifiers_outputs, pair_metric):
    """Calculates the averaged scores in all classifiers.

    Parameters
    ----------
    classifiers_outputs : array-like, shape = (n_classifier, n_samples)
        The N arrays for each classifier as defined in
        (Kuncheva & Whitaker, 2003), https://doi.org/10.1023/A:102285900
    pair_metric : function
        Any function which calculates pair-wise diversity measure.

    Returns
    -------
    score : float
        The averaged diversity score.
    """
    n_classifiers = classifiers_outputs.shape[0]
    factor = 2.0 / float(n_classifiers * (n_classifiers - 1))
    sum_ = 0.0
    for i in range(n_classifiers - 1):
        for k in range(i + 1, n_classifiers):
            sum_ += pair_metric(classifiers_outputs, i, k)
    return factor * sum_

# pair_wise classifier diversity metrics

def yule_diversity_pair_wise(classifiers_outputs, ind_i, ind_k):
    """Calculates the pair-wise Yule's Q statistic.

    Parameters
    ----------
    classifiers_outputs : array-like, shape = (n_classifier, n_samples)
        The N arrays for each classifier as defined in
        (Kuncheva & Whitaker, 2003), https://doi.org/10.1023/A:102285900
    ind_i : int
        The index of the first classifier in the classifiers_outputs array.
    ind_k : int
        The index of the second classifier in the classifiers_outputs array.

    Returns
    -------
    score : float
        The pair-wise diversity score between classifiers i and k.
    """
    n_tab = relationship_table(classifiers_outputs, ind_i, ind_k)
    return (float(n_tab[1, 1] * n_tab[0, 0] - n_tab[0, 1] * n_tab[1, 0]) /
            float(n_tab[1, 1] * n_tab[0, 0] + n_tab[0, 1] * n_tab[1, 0]))

def double_fault_pair_wise(classifiers_outputs, ind_i, ind_k):
    """Calculates the pair-wise double fault diversity measure.

    Parameters
    ----------
    classifiers_outputs : array-like, shape = (n_classifier, n_samples)
        The N arrays for each classifier as defined in
        (Kuncheva & Whitaker, 2003), https://doi.org/10.1023/A:102285900
    ind_i : int
        The index of the first classifier in the classifiers_outputs array.
    ind_k : int
        The index of the second classifier in the classifiers_outputs array.

    Returns
    -------
    score : float
        The pair-wise diversity score between classifiers i and k.
    """
    n_tab = relationship_table(classifiers_outputs, ind_i, ind_k)
    return (float(n_tab[0, 0]) /
            float(n_tab[1, 1] + n_tab[1, 0] + n_tab[0, 1] + n_tab[0, 0]))

def disagreement_measure_pair_wise(classifiers_outputs, ind_i, ind_k):
    """Calculates the pair-wise disagreement diversity measure.

    Parameters
    ----------
    classifiers_outputs : array-like, shape = (n_classifier, n_samples)
        The N arrays for each classifier as defined in
        (Kuncheva & Whitaker, 2003), https://doi.org/10.1023/A:102285900
    ind_i : int
        The index of the first classifier in the classifiers_output array.
    ind_k : int
        The index of the second classifier in the classifiers_output array.

    Returns
    -------
    score : float
        The pair-wise diversity score between classifiers i and k.
    """
    n_tab = relationship_table(classifiers_outputs, ind_i, ind_k)
    return (float(n_tab[0, 1] + n_tab[1, 0]) /
            float(n_tab[1, 1] + n_tab[1, 0] + n_tab[0, 1] + n_tab[0, 0]))

# ensemble diversity metrics

def yule_diversity(classifiers_outputs):
    """Calculates Yule's Q statistic for each pair of classifiers and
    returns the average.

    Parameters
    ----------
    classifiers_outputs : array-like, shape = (n_classifier, n_samples)
        The N arrays for each classifier as defined in
        (Kuncheva & Whitaker, 2003), https://doi.org/10.1023/A:102285900

    Returns
    -------
    score : float
        The ensemble's average Yule's Q statistic.
    """
    return q_average(classifiers_outputs, yule_diversity_pair_wise)

def double_fault(classifiers_outputs):
    """Calculates the Double Fault diversity measure for each pair of
    classifiers and returns the average.

    Parameters
    ----------
    classifiers_outputs : array-like, shape = (n_classifier, n_samples)
        The N arrays for each classifier as defined in
        (Kuncheva & Whitaker, 2003), https://doi.org/10.1023/A:102285900

    Returns
    -------
    score : float
        The ensemble's average Double Fault diversity.
    """
    return q_average(classifiers_outputs, double_fault_pair_wise)

def disagreement_measure(classifiers_output):
    """Calculates the disagreement measure for each pair of classifiers and
    returns the average.

    Parameters
    ----------
    classifiers_outputs : array-like, shape = (n_classifier, n_samples)
        The N arrays for each classifier as defined in
        (Kuncheva & Whitaker, 2003), https://doi.org/10.1023/A:102285900

    Returns
    -------
    score : float
        The ensemble's average disagreement measure for diversity.
    """
    return q_average(classifiers_output, disagreement_measure_pair_wise)

def entropy_measure(classifiers_outputs):
    """Calculates the entropy (E) diversity measure for the ensemble.

    Parameters
    ----------
    classifiers_outputs : array-like, shape = (n_classifier, n_samples)
        The N arrays for each classifier as defined in
        (Kuncheva & Whitaker, 2003), https://doi.org/10.1023/A:102285900

    Returns
    -------
    score : float
        The ensemble's entroy diversity score.
    """
    sum_ = 0.0
    n_classifiers = classifiers_outputs.shape[0]
    n_examples = classifiers_outputs.shape[1]
    mult = 1.0 / float(n_classifiers - math.ceil(float(n_classifiers)/2.0))
    for example in range(n_examples):
        l_sum = np.sum(classifiers_outputs[:, example])
        sum_ += mult * min(l_sum, n_classifiers - l_sum)
    return sum_ / n_examples

def kohavi_wolpert(classifiers_outputs):
    """Calculates the Kohavi-Wolpert (KW) diversity measure for the ensemble.

    Parameters
    ----------
    classifiers_outputs : array-like, shape = (n_classifier, n_samples)
        The N arrays for each classifier as defined in
        (Kuncheva & Whitaker, 2003), https://doi.org/10.1023/A:102285900

    Returns
    -------
    score : float
        The ensemble's KW diversity score.
    """
    sum_ = 0.0
    n_classifiers = classifiers_outputs.shape[0]
    n_examples = classifiers_outputs.shape[1]
    for example in range(n_examples):
        l_sum = np.sum(classifiers_outputs[:, example])
        sum_ += l_sum * (n_classifiers - l_sum)
    return sum_ / (n_examples * n_classifiers * n_classifiers)

DIVERSITY_METRIC_MAP = {
    "yule": yule_diversity,
    "double_fault": double_fault,
    "disagreement": disagreement_measure,
    "entropy": entropy_measure,
    "kw": kohavi_wolpert
}

MIN_DIVERSITY_METRIC_MAP = {
    "yule": 1.0,
    "double_fault": 1.0,
    "disagreement": 0.0,
    "entropy": 0.0,
    "kw": 0.0
}
