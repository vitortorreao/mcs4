#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Provides methods for pruning the pool of classifiers."""

import math
import numpy as np

from .ensemble import EnsembleBasic

# helper functions (for easy unit testing)

def count_votes(predictions, classes, class_index_map):
    """Computes the number of votes for each class in each sample."""
    n_samples = predictions.shape[0]
    n_classifiers = predictions.shape[1]
    n_classes = classes.shape[0]
    votes = np.zeros((n_samples, n_classes), dtype=np.int)
    for i_classifier in range(n_classifiers):
        for j_sample in range(n_samples):
            class_i = class_index_map[predictions[j_sample, i_classifier]]
            votes[j_sample, class_i] += 1
    return votes

def get_predictions(ensemble, validation_examples):
    """Calculates the prediction matrix so we don't have to run the classifiers
    more than once"""
    n_samples = validation_examples.shape[0]
    n_classifiers = len(ensemble.classifiers)
    predictions = np.zeros((n_samples, n_classifiers), dtype=np.int)
    for col in range(n_classifiers):
        new_col = ensemble.classifiers[col].predict(validation_examples)
        predictions[:, col] = new_col
    return predictions

def get_individual_contribution(j_sample, i_classifier, votes, predictions,
                                classes, validation_labels, class_index_map):
    """Calculates the individual contribution of a classifier on the sample"""
    alpha = 0
    beta = 0
    theta = 0
    max_class = np.argmax(votes[j_sample, :])
    min_class = np.argmin(votes[j_sample, :])
    v_max = votes[j_sample, max_class]
    v_correct = votes[j_sample, class_index_map[validation_labels[j_sample]]]
    sorted_votes = list(votes[j_sample, :])
    sorted_votes.sort()
    v_sec = sorted_votes[-2]
    v_ci = votes[j_sample, class_index_map[predictions[j_sample, i_classifier]]]
    # print("=================================================")
    # print("Correct class = " + str(validation_labels[j_sample]))
    # print("Classifier prediction = " + str(predictions[j_sample, i_classifier]))
    # print("Max class = " + str(classes[max_class]))
    # print("Min class = " + str(classes[min_class]))
    if (predictions[j_sample, i_classifier] == validation_labels[j_sample]):
        if (predictions[j_sample, i_classifier] == classes[max_class]):
            beta = 1
        else:
            alpha = 1
    else:
        theta = 1
    # print(alpha, beta, theta)
    return alpha * (2 * v_max - v_ci) + \
           beta * v_sec + \
           theta * (v_correct - v_ci - v_max)

# actual pruning functions!

def epic_pruning(ensemble, p, validation_examples, validation_labels):
    """Pruns the pool of classifiers in the ensemble using the EPIC algorithm

    Parameters
    ----------
    ensemble : BasicEnsemble
        Some instance of BasicEnsemble, where the classifiers are already
        trained.
    p : float
        The percentage of classifiers that the resulting subensemble must have.
    validation_examples : array-like, shape (n_examples, n_features)
        The validation dataset to be used for the prunning process.
    validation_labels : array-like, shape (n_examples,)
        The correct labels for the validation dataset.
    """
    if not isinstance(ensemble, EnsembleBasic):
        raise ValueError("The 'ensemble' parameter must be an instance of " +
                         "EnsembleBasic.")
    n_classifiers = len(ensemble.classifiers)
    n_samples = validation_examples.shape[0]
    classes = np.unique(validation_labels)
    n_classes = classes.shape[0]
    class_index_map = dict()
    for index in range(n_classes):
        class_index_map[classes[index]] = index
    predictions = get_predictions(ensemble, validation_examples)
    votes = count_votes(predictions, classes, class_index_map)
    classifiers = list()
    for i_classifier in range(n_classifiers):
        sum_ = 0
        for j_sample in range(n_samples):
            sum_ += get_individual_contribution(j_sample, i_classifier, votes,
                                                predictions, classes,
                                                validation_labels,
                                                class_index_map)
        classifiers.append((ensemble.classifiers[i_classifier], sum_))
    classifiers = sorted(classifiers, key=lambda x: x[1], reverse=True)
    ensemble.classifiers = list()
    result_n = int(math.floor(n_classifiers * p))
    for i in range(result_n):
        ensemble.classifiers.append(classifiers[i][0])
