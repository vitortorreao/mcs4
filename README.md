# MCS - Lista 4

Este documento contém as informações necessárias para executar os scripts
referentes a cada questão da lista.

## Instalação

Esta seção lida com o que precisa ser instalado no seu sistema para execução
dos scripts.

### Windows

As instruções apresentadas aqui foram realizadas em uma máquina com o sistema
operacional Windows 10 (64bits), mas é possível executar em qualquer máquina
Windows adaptando os paços a seguir.

Você deve visitar Python.org e baixar o instalar do Python para o Windows.
Depois, basta instalá-lo no bom e velho next, next, install.

Para se certificar de que o Python foi instalado, abra um prompt de comando e
digite o seguinte comando:

```
> python -V
```

A saída deve ser algo como `Python 2.7.13` (a depender da sua versão). Se isso
não é o que você vê, pode ser que o Python não esteja na sua variável PATH.
Adicione-o e tente novamente. Se ainda houverem erros, você pode procurar ajuda
no portal Python.org.

Antes de prosseguir, é preciso instalar manualmente a biblioteca NumPy. Isso
porque o NumPy depende da biblioteca MKL no Windows. Existe uma forma fácil de
fazer isso. Vá até o site http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy, que
mantém Python Wheels para o Windows. E faça o download do arquivo:

```
numpy‑1.X.Y+mkl‑cp27‑cp27m‑Z.whl
```

Onde X e Y são as versões do NumPy (o código foi testado utilizando a versão
1.13.1, mas pode ser que esta versão não esteja mais disponível no momento que
você for fazer o download, então baixe a versão mais próxima que estiver
disponível). E Z depende da sua arquitetura: `win_amd64` para Windows 64 bits e
`win32`. Se você instalou o Python de 32 bits, você deve baixar o numpy de 32
bits, assim como se você instalou o Python de 64 bits, você deve baixar o numpy
de 64 bits.

Depois que o download estiver concluído, basta executar:

```
> pip install numpy‑1.X.Y+mkl‑cp27‑cp27m‑Z.whl
```

Você deve fazer o mesmo para o SciPy, que pode ser encontrado em:
http://www.lfd.uci.edu/~gohlke/pythonlibs/#scipy. A versão utilizada no projeto
foi a 0.19.1, portanto, tente baixar uma versão próxima que esteja disponível.

Depois de rodar:

```
> pip install scipy‑0.X.Y‑cp27‑cp27m‑Z.whl
```

você pode prosseguir com os passos abaixo.

### Linux (Ubuntu)

As instruções apresentadas aqui foram realizadas em uma máquina com o sistema
operacional Ubuntu (14.04 LTS), mas é possível executar em qualquer máquina
Linux adaptando os paços a seguir.

Primeiro, você precisa instalar Python na versão 2.7. Será necessário também,
compilar e instalar algumas bibliotecas e para isso, serão necessários outros
pacotes.

```sh
$ sudo apt-get install python2.7 python2.7-pip python2.7-dev libpython2.7-dev
```

Certifique-se de que Python foi instalado corretamente com o seguinte comando:

```sh
$ python -V
```

A saída deve ser algo como `Python 2.7.6` (a depender da sua versão).

## Bibliotecas

Uma vez que o Python estiver instalado no seu sistema, você deve instalar as
dependências dos scripts.

```sh
$ pip install -r requirements.txt
```

No Windows:

```
> pip install -r requirements.Windows.txt
```

(Lembre-se de instalar o NumPy manualmente no Windows seguindo os passos na
seção de Instalação)

## Execução

Para executar cada script que responde a segunda questão, basta executar o
seguinte comando:

```sh
$ ./exercise2.py
```

## Tests unitários

Algumas funcionalidades do projeto possuem testes unitários. Para executá-los,
basta rodar o seguinte commando na pasta raiz do projeto.

```sh
$ nosetests
```
