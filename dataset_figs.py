#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Exercise number 1."""

from mcs.datasets import read_dataset

def main():
    """Function for using this script as an executable. Don't import this."""
    import matplotlib
    matplotlib.use("Svg")
    import matplotlib.pyplot as plt
    from matplotlib.ticker import MaxNLocator
    import os

    figures_dir = "figures"
    if not os.path.exists(figures_dir):
        os.makedirs(figures_dir)
    objects, labels = read_dataset("resources/seeds_dataset.txt",
                                   separator="\t", has_missing=False)
    axis = plt.figure().gca()
    axis.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.hist(labels, color='k')
    plt.title("Seeds Dataset - Distribuicao das Classes")
    plt.xlabel("Classe")
    plt.ylabel("Frequencia")
    plt.savefig(os.path.join(figures_dir, "seeds.svg"), format="svg")
    print("The seeds dataset has {0} objects, each with {1} features."
          .format(objects.shape[0], objects.shape[1]))
    objects, labels = read_dataset("resources/transfusion.data", separator=",",
                                   has_header=True)
    plt.figure()
    axis = plt.figure().gca()
    axis.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.hist(labels, color='k')
    plt.title("Transfusion Dataset - Distribuicao das Classes")
    plt.xlabel("Classe")
    plt.ylabel("Numero de Exemplos")
    plt.savefig(os.path.join(figures_dir, "transfusion.svg"), format="svg")
    print("The transfusion dataset has {0} objects, each with {1} features."
          .format(objects.shape[0], objects.shape[1]))
    objects, labels = read_dataset("resources/segmentation.data", separator=",",
                                   has_header=True, class_column=0)
    plt.figure()
    axis = plt.figure().gca()
    axis.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.hist(labels, color='k')
    plt.title("Segmentation Dataset - Distribuicao das Classes")
    plt.xlabel("Classe")
    plt.ylabel("Numero de Exemplos")
    plt.savefig(os.path.join(figures_dir, "segmentation.svg"), format="svg")
    print("The segmentation dataset has {0} objects, each with {1} features."
          .format(objects.shape[0], objects.shape[1]))

if __name__ == "__main__":
    main()
