# Relatório

Para gerar o relatório, você deve ter uma distribuição LaTeX instalada na sua
máquina. Também é necessário instalar o Inkscape (https://inkscape.org/en/) e
adicioná-lo na sua variável de ambiente PATH, uma vez que as imagens geradas
são em formato SVG (vetorial) e o pacote SVG do LaTeX precisa dessa ferramenta.

Para que o documento apresente as imagens, é necessário executar os scripts
Python antes. Eles irão popular uma pasta chamada `figures` com as imagens
utilizadas no relatório. Depois disso, basta compilar o documento utilizando o
`PDFLaTeX`.