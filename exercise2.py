"""Executes the experiment for the second question in the 4th exercise list"""

import os
import random
import numpy as np

from mcs.datasets import read_dataset
from mcs.ensemble import BaggingBasic
from mcs.selection import BaggingOLA, BaggingLCA, BaggingKNORAE
from sklearn.metrics import accuracy_score
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.naive_bayes import GaussianNB

import matplotlib
matplotlib.use("Svg")
import matplotlib.pyplot as plt

class NaiveBayesOLABagging(BaggingOLA):
    """Represents the bagging ensemble of Naive Bayes with OLA dynamic
    selection"""

    def __init__(self, n_classifiers=1, k_neighbors=5):
        """Creates a new instance of the classifier."""
        super(NaiveBayesOLABagging, self).__init__(n_classifiers, k_neighbors)
        for i in range(n_classifiers):
            self.classifiers[i] = GaussianNB()

class NaiveBayesLCABagging(BaggingLCA):
    """Represents the bagging ensemble of Naive Bayes with LCA dynamic
    selection"""

    def __init__(self, n_classifiers=1, k_neighbors=5):
        """Creates a new instance of the classifier."""
        super(NaiveBayesLCABagging, self).__init__(n_classifiers, k_neighbors)
        for i in range(n_classifiers):
            self.classifiers[i] = GaussianNB()

class NaiveBayesKNORAEBagging(BaggingKNORAE):
    """Represents the bagging ensemble of Naive Bayes with KNORA-E dynamic
    selection"""

    def __init__(self, n_classifiers=1, k_neighbors=5):
        """Creates a new instance of the classifier."""
        super(NaiveBayesKNORAEBagging, self).__init__(n_classifiers, k_neighbors)
        for i in range(n_classifiers):
            self.classifiers[i] = GaussianNB()

class NaiveBayesBagging(BaggingBasic):
    """Represents the bagging ensemble of Naive Bayes."""

    def __init__(self, n_classifiers=1):
        """Creates a new instance of the classifier."""
        super(NaiveBayesBagging, self).__init__(n_classifiers)
        for i in range(n_classifiers):
            self.classifiers[i] = GaussianNB()

def comp(ensembler, objects, labels, kneighbors, dataset_name, dynamic_name,
         figures_dir):
    pool_sizes = range(10, 110, 10)
    ensemble_accuracies = list()
    select_accuracies = list()
    for pool_size in pool_sizes:
        splits = StratifiedShuffleSplit(n_splits=10, random_state=9876)
        ensemble_scores = list()
        select_scores = list()
        for train_ind, test_ind in splits.split(objects, labels):
            X_train, X_test = objects[train_ind], objects[test_ind]
            y_train, y_test = labels[train_ind], labels[test_ind]
            nb = NaiveBayesBagging(n_classifiers=pool_size)
            nb.fit(X_train, y_train)
            y_pred = nb.predict(X_test)
            ensemble_scores.append(accuracy_score(y_test, y_pred))
            val_splits = StratifiedShuffleSplit(n_splits=1, test_size=0.3,
                                                random_state=6789)
            for train2_ind, val_ind in val_splits.split(X_train, y_train):
                X_train, X_val = X_train[train2_ind], X_train[val_ind]
                y_train, y_val = y_train[train2_ind], y_train[val_ind]
            nb2 = ensembler(n_classifiers=pool_size, k_neighbors=kneighbors)
            nb2.fit(X_train, y_train)
            nb2.calculate_guesses(X_val, y_val)
            y_pred2 = nb2.predict(X_test)
            select_scores.append(accuracy_score(y_test, y_pred2))
        ensemble_accuracies.append(np.mean(ensemble_scores))
        select_accuracies.append(np.mean(select_scores))
    plt.figure()
    plt.plot(pool_sizes, ensemble_accuracies, 'k', label="Sem selecao dinamica")
    plt.plot(pool_sizes, select_accuracies, 'k', linestyle='--',
             label="{0} dynamic selection".format(dynamic_name))
    plt.legend()
    plt.xticks(pool_sizes)
    plt.title("{0} Dataset - Naive Bayes Pool (Bagging)".format(dataset_name))
    plt.xlabel("Tamanho do Pool")
    plt.ylabel("Taxa de Acerto Media em 10 folds")
    plt.savefig(os.path.join(figures_dir,
                             "{0}_bagging_nb_{1}.svg".format(dataset_name,
                                                             dynamic_name)),
                format="svg")
    for ps_index in range(len(pool_sizes)):
        print(("Average accuracy for an ensemble (size = {0}) generated " +
               "without dynamic selection in the {1} dataset: {2}").format(
                   pool_sizes[ps_index],
                   dataset_name,
                   ensemble_accuracies[ps_index])
             )
        print(("Average accuracy for an ensemble (size = {0}) generated " +
               "with {1} dynamic selection in the {2} dataset: {3}").format(
                   pool_sizes[ps_index],
                   dynamic_name,
                   dataset_name,
                   select_accuracies[ps_index])
             )

def main():
    random.seed(2000)
    figures_dir = "figures"
    if not os.path.exists(figures_dir):
        os.makedirs(figures_dir)
    trans_objects, trans_labels = read_dataset("resources/transfusion.data",
                                               separator=",",
                                               has_header=True)
    seeds_objects, seeds_labels = read_dataset("resources/seeds_dataset.txt",
                                               separator="\t",
                                               has_missing=False)
    segme_objects, segme_labels = read_dataset("resources/segmentation.data",
                                               separator=",",
                                               has_header=True,
                                               class_column=0)
    comp(NaiveBayesOLABagging, trans_objects, trans_labels, 7, "Transfusion",
         "OLA", figures_dir)
    comp(NaiveBayesOLABagging, seeds_objects, seeds_labels, 7, "Seeds",
         "OLA", figures_dir)
    comp(NaiveBayesOLABagging, segme_objects, segme_labels, 7, "Segmentation",
         "OLA", figures_dir)
    comp(NaiveBayesLCABagging, trans_objects, trans_labels, 7, "Transfusion",
         "LCA", figures_dir)
    comp(NaiveBayesLCABagging, seeds_objects, seeds_labels, 7, "Seeds",
         "LCA", figures_dir)
    comp(NaiveBayesLCABagging, segme_objects, segme_labels, 7, "Segmentation",
         "LCA", figures_dir)
    comp(NaiveBayesKNORAEBagging, trans_objects, trans_labels, 7, "Transfusion",
         "KNORA-E", figures_dir)
    comp(NaiveBayesKNORAEBagging, seeds_objects, seeds_labels, 7, "Seeds",
         "KNORA-E", figures_dir)
    comp(NaiveBayesKNORAEBagging, segme_objects, segme_labels, 7,
         "Segmentation", "KNORA-E", figures_dir)

if __name__ == "__main__":
    main()
